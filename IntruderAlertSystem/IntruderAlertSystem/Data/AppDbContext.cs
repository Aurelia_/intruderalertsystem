﻿using IntruderAlertSystem.Helpers;
using IntruderAlertSystem.Models;
using System.Data.Entity;

namespace IntruderAlertSystem.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base(Constants.ConnectionString) { }
        public DbSet<Users> ApplicationUsers { get; set; }
        public DbSet<Alerts> Alerts { get; set; }
    }
}
