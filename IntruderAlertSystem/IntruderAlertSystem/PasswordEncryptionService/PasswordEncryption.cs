﻿namespace IntruderAlertSystem.PasswordEncryptionService
{
    public static class PasswordEncryption
    {
        public static string Encrypt(string password, string salt)
        {
            return Hash.Create(password, salt);
        }

        public static bool PasswordIsCorrect(string password, string salt, string encryptedPassword)
        {
            return (Encrypt(password, salt) == encryptedPassword);
        }
    }
}
