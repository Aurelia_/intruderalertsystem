﻿using System;

namespace IntruderAlertSystem.Models
{
    public class Alerts
    {
        public Guid Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
        public Users User { get; set; }
        public bool IsActive { get; set; }
    }
}
