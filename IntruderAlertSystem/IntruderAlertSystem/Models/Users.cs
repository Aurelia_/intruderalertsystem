﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IntruderAlertSystem.Models
{
    public class Users
    {
        public Guid Id { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Salt { get; set; }
    }
}
