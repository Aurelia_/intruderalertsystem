﻿using IntruderAlertSystem.Helpers;
using System;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace IntruderAlertSystem.Forms
{
    public partial class Dashboard : Form
    {
        private string ActivateAlarmText = "Alarm is deactivated";
        private string DeactivateAlarmText = "Deactivate alarm";
        private string SystemOnText = "Turn on the security system";
        private string SystemOffText = "Turn off the security system";

        public Dashboard()
        {
            InitializeComponent();
            this.label1.Text = "Welcome, " + SessionHelper.getCurrentUser().Email + "!";
            serialPort.PortName = Constants.PortName;
            serialPort.Open();
            Button.CheckForIllegalCrossThreadCalls = false;
            setAlarmInfo();
            setSystemInfo();
            // check the state of the system
            if (!SessionHelper.SystemIsOn())
            {
                button1.Enabled = false;
                button2.Text = SystemOnText;
            }
            else
            {
                if (SessionHelper.AlarmIsOn())
                {
                    button1.Enabled = true;
                    button1.Text = DeactivateAlarmText;
                    button2.Enabled = false;
                    button2.Text = SystemOffText;
                }
                else
                {
                    button1.Enabled = false;
                    button1.Text = ActivateAlarmText;
                    button2.Enabled = true;
                    button2.Text = SystemOffText;
                }
            }
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.serialPort.Close();
            SessionHelper.UnsetUser();
            Login login = new Login();
            login.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // toggle the system state
            if (SessionHelper.SystemIsOn())
            {
                if (SessionHelper.AlarmIsOn())
                {
                    SessionHelper.ToggleAlarmState();
                    setAlarmInfo();
                    AlertsHelper.GetInstance().UpdateAlarm();
                    button2.Enabled = true;
                    button1.Enabled = false;
                    button1.Text = ActivateAlarmText;
                    serialPort.Write(Constants.SystemOnAlarmOff);
                    SessionHelper.setCurrentState(Constants.SystemOnAlarmOff);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // toggle the system state
            if (!SessionHelper.AlarmIsOn())
            {
                SessionHelper.ToggleSystemState();
                setSystemInfo();
                if (SessionHelper.SystemIsOn())
                {
                    // then the user can activate/deactivate the alarm
                    button1.Enabled = false;
                    button1.Text = DeactivateAlarmText;

                    serialPort.Write(Constants.SystemOnAlarmOff); // 0
                    SessionHelper.setCurrentState(Constants.SystemOnAlarmOff);
                    button2.Text = SystemOffText;
                }
                else
                {
                    button1.Enabled = false;
                    button1.Text = DeactivateAlarmText;
                    button2.Text = SystemOnText;
                    serialPort.Write(Constants.SystemOffAlarmOff); //1
                    SessionHelper.setCurrentState(Constants.SystemOffAlarmOff);

                }
            }
        }

        private void setAlarmInfo()
        {
            infoTextBoxAlarm.Text = "The alarm is: " + (SessionHelper.AlarmIsOn() ? "on." : "off.");

        }

        private void setSystemInfo()
        {
            infoTextBox.Text = "The security system is: " + (SessionHelper.SystemIsOn() ? "on." : "off.");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.serialPort.Close();
            Alerts alerts = new Alerts();
            this.Hide();
            alerts.Show();
        }

        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            var port = (SerialPort)sender;
            string data = port.ReadExisting();
            string currentState = SessionHelper.getCurrentState();
            int asciiCode = data.ElementAt(0);
            string ch = ((char)asciiCode).ToString();
            bool cond1 = ch.Equals(Constants.SystemOnAlarmOn);
            bool cond2 = !(data.Equals(currentState));

            if (cond1 && cond2)
            {
                if (!SessionHelper.AlarmIsOn())
                {
                    button2.Enabled = false;
                    button1.Text = DeactivateAlarmText;
                    button1.Enabled = true;
                    SessionHelper.ToggleAlarmState();
                    setAlarmInfo();
                    SessionHelper.setCurrentState(Constants.SystemOnAlarmOn);
                    // then on/off button for the system is disabled 
                    

                    AlertsHelper helper = AlertsHelper.GetInstance();
                    helper.HandleAlarm();
                    Console.WriteLine(SessionHelper.AlarmIsOn());

                }
            }
        }

        private void Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                serialPort.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
