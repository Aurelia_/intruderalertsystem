﻿using IntruderAlertSystem.Data;
using IntruderAlertSystem.Helpers;
using IntruderAlertSystem.Models;
using IntruderAlertSystem.PasswordEncryptionService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;

namespace IntruderAlertSystem.Forms
{
    public partial class Register : Form
    {
        private string firstNameErrorMsg;
        private string lastNameErrorMsg;
        private string emailErrorMsg;
        private string passwordErrorMsg;

        public Register()
        {
            InitializeComponent();
            serialPort.PortName = Constants.PortName;
            serialPort.Open();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            if(!UserExists())
            {
                RegisterUser();
                firstNameTextBox.Text = null;
                lastNameTextBox.Text = null;
                emailTextBox.Text = null;
                passwordTextBox.Text = null;
                Login login = new Login();
                this.Hide();
                login.Show();
            }
            else
            {
                string message = "A user with the same email already exists! Please enter another email address";
                MessageBox.Show(message, "User already exists", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterUser()
        {
            var newUser = CreateUser();
            using (var dbContext = new AppDbContext())
            {
                dbContext.ApplicationUsers.Add(newUser);
                dbContext.SaveChanges();
            }
        }


        private Users CreateUser()
        {
            string plainPassword = passwordTextBox.Text;

            string salt = Salt.Create();
            string encryptedPassword = PasswordEncryption.Encrypt(plainPassword, salt);

            return new Users
            {
                Id = Guid.NewGuid(),
                FirstName = firstNameTextBox.Text,
                LastName = lastNameTextBox.Text,
                Email = emailTextBox.Text,
                Password = encryptedPassword,
                Salt = salt
            };
        }

        private bool UserExists()
        {
            Users searchedUser = null;
            var allUsers = new List<Users>();

            using (var dbContext = new AppDbContext())
            {
                allUsers = dbContext.ApplicationUsers.ToList();
            }
            foreach(var user in allUsers)
            {
                if (user.Email.Equals(emailTextBox.Text))
                {
                    searchedUser = user;
                    break;
                }
            }
            return (searchedUser != null);
        }

        private void EmailTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string errorMsg;
            if (!EmailAddressIsValid(out errorMsg))
            {
                e.Cancel = true;
                this.emailErrorProvider.SetError(emailTextBox, errorMsg);
            }
            else
            {
                this.emailErrorProvider.SetError(emailTextBox, "");
            }
        }


        public bool EmailAddressIsValid(out string errorMessage)
        {
            if (emailTextBox.Text.Length == 0)
            {
                errorMessage = "email address is required.";
                return false;
            }

            if (emailTextBox.Text.IndexOf("@") > -1)
            {
                if (emailTextBox.Text.IndexOf(".", emailTextBox.Text.IndexOf("@")) > emailTextBox.Text.IndexOf("@"))
                {
                    errorMessage = "";
                    return true;
                }
            }

            errorMessage = "Invalid email address!";
            return false;
        }


        private void FirstNameTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string errorMsg;
            if (!FirstNameIsValid(out errorMsg))
            {
                this.firstNameErrorProvider.SetError(firstNameTextBox, errorMsg);
            }
            else
            {
                this.firstNameErrorProvider.SetError(firstNameTextBox, "");
            }
        }

        private bool FirstNameIsValid(out string errorMessage)
        {
            if (firstNameTextBox.Text.Length == 0)
            {
                errorMessage = "First name is required.";
                return false;
            }

            else if (firstNameTextBox.Text.Length < 3)
            {
                errorMessage = "Invalid text";
                return false;
            }
            errorMessage = "";
            return true;
        }


        private void LastNameTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string errorMsg;
            if (!LastNameIsValid(out errorMsg))
            {
                this.lastNameErrorProvider.SetError(lastNameTextBox, errorMsg);
            }
            else
            {
                this.lastNameErrorProvider.SetError(lastNameTextBox, "");
            }
        }

        private bool LastNameIsValid(out string errorMessage)
        {
            if (lastNameTextBox.Text.Length == 0)
            {
                errorMessage = "Last name is required.";
                return false;
            }

            else if (lastNameTextBox.Text.Length < 3)
            {
                errorMessage = "Invalid text";
                return false;
            }
            errorMessage = "";
            return true;
        }

        private void PasswordTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string errorMsg;
            if (!PasswordIsValid(out errorMsg))
            {
                this.passwordErrorProvider.SetError(passwordTextBox, errorMsg);
            }
            else
            {
                this.passwordErrorProvider.SetError(passwordTextBox, "");
            }
        }

        private bool PasswordIsValid(out string errorMessage)
        {
            if(!passwordTextBox.Text.Any(char.IsDigit))
            {
                errorMessage = "Password must contain a digit!";
                return false;
            }

            if(!passwordTextBox.Text.Any(char.IsUpper))
            {
                errorMessage = "Password must contain an upper letter!";
                return false;
            }

            if (passwordTextBox.Text.Length < 8)
            {
                errorMessage = "Password must have at least 8 characters!";
                return false;
            }

            errorMessage = "";
            return true;
        }

        private void EnableOrDisableSubmitButton()
        {
            if (FormIsValid())
            {
                RegisterButton.Enabled = true;
                RegisterButton.BackColor = Color.Blue;
            }
            else
            {
                RegisterButton.Enabled = false;
                RegisterButton.BackColor = Color.Red;
                RegisterButton.ForeColor = Color.White;
            }
        }

        private bool FormIsValid()
        {
            return (EmailAddressIsValid(out emailErrorMsg) && FirstNameIsValid(out firstNameErrorMsg) && 
                LastNameIsValid(out lastNameErrorMsg) && PasswordIsValid(out passwordErrorMsg));
        }

        private void FirstNameTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableOrDisableSubmitButton();
        }

        private void LastNameTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableOrDisableSubmitButton();
        }

        private void EmailTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableOrDisableSubmitButton();
        }

        private void PasswordTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableOrDisableSubmitButton();
        }

        private void LoginLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            serialPort.Close();
            Login login = new Login();
            this.Hide();
            login.Show();
        }

        private void Register_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort.Close();
        }

        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            var port = (SerialPort)sender;
            string data = port.ReadExisting();
            string currentState = SessionHelper.getCurrentState();
            int asciiCode = data.ElementAt(0);
            string ch = ((char)asciiCode).ToString();
            bool cond1 = ch.Equals(Constants.SystemOnAlarmOn);
            bool cond2 = !(data.Equals(currentState));

            if (cond1 && cond2)
            {
                SessionHelper.setCurrentState(data);
                if (!SessionHelper.AlarmIsOn())
                {
                    SessionHelper.ToggleAlarmState();
                    AlertsHelper helper = AlertsHelper.GetInstance();
                    helper.HandleAlarm();
                }
            }
        }
    }
}
