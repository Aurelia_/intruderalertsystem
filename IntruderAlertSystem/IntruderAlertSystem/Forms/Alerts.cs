﻿using IntruderAlertSystem.Data;
using IntruderAlertSystem.Helpers;
using IntruderAlertSystem.Models;
using IntruderAlertSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;

namespace IntruderAlertSystem.Forms
{
    public partial class Alerts : Form
    {
        public Alerts()
        {
            InitializeComponent();
            SeedDataGridView();
            serialPort.PortName = Constants.PortName;
            serialPort.Open();
        }

        private void SeedDataGridView()
        {
            var alertsVm = GetAllAlerts();

            alertsDataGridView.Rows.Clear();

            foreach(var alertVm in alertsVm)
            {
                DataGridViewRow newRow = new DataGridViewRow();
                newRow.CreateCells(alertsDataGridView);
                newRow.Cells[0].Value = alertVm.StartDate;
                newRow.Cells[1].Value = alertVm.EndDate;
                newRow.Cells[2].Value = alertVm.StoppedBy;

                alertsDataGridView.Rows.Add(newRow);
            }
        }

        private IEnumerable<AlertVm> GetAllAlerts()
        {
            var alerts = new List<IntruderAlertSystem.Models.Alerts>();
            Users user = null;

            using (var dbContext = new AppDbContext())
            {
                alerts = dbContext.Set<IntruderAlertSystem.Models.Alerts>()
                    .ToList();
                string email = SessionHelper.getLastLoggedUserEmail();
                user = dbContext.Set<Users>().Where(x => x.Email.Equals(email)).FirstOrDefault();
            }

            var alertsVm = new List<AlertVm>();
            foreach(var alert in alerts)
            {
                if (alert.User.Id == user.Id)
                {
                    var alertVm = new AlertVm
                    {
                        EndDate = alert.StopDate,
                        StartDate = alert.StartDate,
                        StoppedBy = user.FirstName + " " + user.LastName
                    };
                    alertsVm.Add(alertVm);
                }
            }
            return alertsVm;
        }

        private void RefreshAlertsButton_Click(object sender, EventArgs e)
        {
            SeedDataGridView();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            SessionHelper.UnsetUser();
            Login login = new Login();
            login.Show();
            this.Hide();
            serialPort.Close();
        }

        private void goToDashboardButton_Click(object sender, EventArgs e)
        {
            serialPort.Close();
            Dashboard dashboardForm = new Dashboard();
            this.Hide();
            dashboardForm.Show();
        }

        private void Alerts_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort.Close();
        }

        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            var port = (SerialPort)sender;
            string data = port.ReadExisting();
            string currentState = SessionHelper.getCurrentState();
            int asciiCode = data.ElementAt(0);
            string ch = ((char)asciiCode).ToString();
            bool cond1 = ch.Equals(Constants.SystemOnAlarmOn);
            bool cond2 = !(data.Equals(currentState));

            if (cond1 && cond2)
            {
                SessionHelper.setCurrentState(data);
                if (!SessionHelper.AlarmIsOn())
                {
                    SessionHelper.ToggleAlarmState();
                    AlertsHelper helper = AlertsHelper.GetInstance();
                    helper.HandleAlarm();
                }
            }
        }
    }
}
