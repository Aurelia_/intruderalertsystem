﻿using IntruderAlertSystem.Data;
using IntruderAlertSystem.Helpers;
using IntruderAlertSystem.PasswordEncryptionService;
using System;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace IntruderAlertSystem.Forms
{
    public partial class Login : Form
    {
        private string emailErrorMsg;
        private string passwordErrorMsg;

        public Login()
        {
            InitializeComponent();
            serialPort.PortName = Constants.PortName;
            serialPort.Open();
            serialPort.Write(SessionHelper.getCurrentState());
            
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                string email = emailField.Text;
                string password = passwordField.Text;

                using (var dbContext = new AppDbContext())
                {
                    var user = dbContext.ApplicationUsers.SingleOrDefault(u => u.Email.Equals(email));

                    if (PasswordEncryption.PasswordIsCorrect(password, user.Salt, user.Password))
                    {
                        // the login is successfull and the user is redirected to the dashboard and their info is stored in the SessionHelper
                        SessionHelper.SetUser(user);

                        // redirect
                        serialPort.Close();
                        Dashboard dashboard = new Dashboard();
                        this.Hide();
                        dashboard.Show();
                    }
                    else
                    {
                        MessageBox.Show("Invalid login attempt. Please try again...");
                    }
                }
            }
            
        }

        private void EmailField_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string errorMsg;
            if (!EmailAddressIsValid(out errorMsg))
            {
                e.Cancel = true;
                this.emailErrorProvider.SetError(emailField, errorMsg);
            }
            else
            {
                this.emailErrorProvider.SetError(emailField, "");
            }
        }


        public bool EmailAddressIsValid(out string errorMessage)
        {
            if (emailField.Text.Length == 0)
            {
                errorMessage = "The email address is required.";
                return false;
            }

            if (emailField.Text.IndexOf("@") > -1)
            {
                if (emailField.Text.IndexOf(".", emailField.Text.IndexOf("@")) > emailField.Text.IndexOf("@"))
                {
                    errorMessage = "";
                    return true;
                }
            }

            errorMessage = "Invalid email address!";
            return false;
        }


        private void Password_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string errorMsg;
            if (!PasswordIsValid(out errorMsg))
            {
                this.passwordErrorProvider.SetError(passwordField, errorMsg);
            }
            else
            {
                this.passwordErrorProvider.SetError(passwordField, "");
            }
        }

        public bool PasswordIsValid(out string errorMessage)
        {
            if (string.IsNullOrEmpty(emailField.Text))
            {
                errorMessage = "The password is required.";
                return false;
            }
            else
            {
                errorMessage = "";
                return true;
            }
        }

        private void EmailField_TextChanged(object sender, EventArgs e)
        {
            EnableOrDisableSubmitButton();
        }
        private void PasswordField_TextChanged(object sender, EventArgs e)
        {
            EnableOrDisableSubmitButton();
        }


        private void EnableOrDisableSubmitButton()
        {
            if (FormIsValid())
            {
                loginButton.Enabled = true;
                loginButton.BackColor = Color.Blue;
            }
            else
            {
                loginButton.Enabled = false;
                loginButton.BackColor = Color.Red;
                loginButton.ForeColor = Color.White;
            }
        }

        private bool FormIsValid()
        {
            return (EmailAddressIsValid(out emailErrorMsg) && PasswordIsValid(out passwordErrorMsg));
        }

        private void registerLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            serialPort.Close();
            Register registerForm = new Register();
            
            this.Hide();
            registerForm.Show();

        }

        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            var port = (SerialPort)sender;
            string data = port.ReadExisting();
            string currentState = SessionHelper.getCurrentState();
            if (data.Equals(Constants.SystemOnAlarmOn) && !data.Equals(currentState))
            {
                if (!SessionHelper.AlarmIsOn())
                {
                    SessionHelper.ToggleAlarmState();
                    SessionHelper.setCurrentState(Constants.SystemOnAlarmOn);
                    // then on/off button for the system is disabled 


                    AlertsHelper helper = AlertsHelper.GetInstance();
                    helper.HandleAlarm();
                    Console.WriteLine(SessionHelper.AlarmIsOn());

                }
            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                serialPort.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
