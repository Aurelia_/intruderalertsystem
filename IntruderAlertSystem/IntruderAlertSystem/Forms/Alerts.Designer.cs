﻿namespace IntruderAlertSystem.Forms
{
    partial class Alerts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alerts));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.refreshAlertsButton = new System.Windows.Forms.Button();
            this.alertsDataGridView = new System.Windows.Forms.DataGridView();
            this.StartedAt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoppedAt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoppedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logoutButton = new System.Windows.Forms.Button();
            this.goToDashboardButton = new System.Windows.Forms.Button();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alertsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(324, 692);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.ImageLocation = "Images";
            this.pictureBox1.Location = new System.Drawing.Point(90, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(139, 135);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(21, 235);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 65);
            this.label1.TabIndex = 0;
            this.label1.Text = "All alerts";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.goToDashboardButton);
            this.panel2.Controls.Add(this.refreshAlertsButton);
            this.panel2.Controls.Add(this.alertsDataGridView);
            this.panel2.Location = new System.Drawing.Point(394, 69);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(751, 611);
            this.panel2.TabIndex = 3;
            // 
            // refreshAlertsButton
            // 
            this.refreshAlertsButton.Location = new System.Drawing.Point(559, 540);
            this.refreshAlertsButton.Name = "refreshAlertsButton";
            this.refreshAlertsButton.Size = new System.Drawing.Size(156, 47);
            this.refreshAlertsButton.TabIndex = 1;
            this.refreshAlertsButton.Text = "Refresh";
            this.refreshAlertsButton.UseVisualStyleBackColor = true;
            this.refreshAlertsButton.Click += new System.EventHandler(this.RefreshAlertsButton_Click);
            // 
            // alertsDataGridView
            // 
            this.alertsDataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.alertsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.alertsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StartedAt,
            this.StoppedAt,
            this.StoppedBy});
            this.alertsDataGridView.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.alertsDataGridView.Location = new System.Drawing.Point(40, 37);
            this.alertsDataGridView.Name = "alertsDataGridView";
            this.alertsDataGridView.RowHeadersWidth = 51;
            this.alertsDataGridView.RowTemplate.Height = 24;
            this.alertsDataGridView.Size = new System.Drawing.Size(675, 497);
            this.alertsDataGridView.TabIndex = 0;
            // 
            // StartedAt
            // 
            this.StartedAt.HeaderText = "Started At";
            this.StartedAt.MinimumWidth = 6;
            this.StartedAt.Name = "StartedAt";
            this.StartedAt.ReadOnly = true;
            this.StartedAt.Width = 150;
            // 
            // StoppedAt
            // 
            this.StoppedAt.HeaderText = "Stopped At";
            this.StoppedAt.MinimumWidth = 6;
            this.StoppedAt.Name = "StoppedAt";
            this.StoppedAt.ReadOnly = true;
            this.StoppedAt.Width = 150;
            // 
            // StoppedBy
            // 
            this.StoppedBy.HeaderText = "Stopped By";
            this.StoppedBy.MinimumWidth = 6;
            this.StoppedBy.Name = "StoppedBy";
            this.StoppedBy.ReadOnly = true;
            this.StoppedBy.Width = 170;
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(1083, 12);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(109, 37);
            this.logoutButton.TabIndex = 4;
            this.logoutButton.Text = "Log out";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // goToDashboardButton
            // 
            this.goToDashboardButton.Location = new System.Drawing.Point(40, 541);
            this.goToDashboardButton.Name = "goToDashboardButton";
            this.goToDashboardButton.Size = new System.Drawing.Size(149, 44);
            this.goToDashboardButton.TabIndex = 5;
            this.goToDashboardButton.Text = "Go to Dashboard";
            this.goToDashboardButton.UseVisualStyleBackColor = true;
            this.goToDashboardButton.Click += new System.EventHandler(this.goToDashboardButton_Click);
            // 
            // serialPort
            // 
            this.serialPort.PortName = "COM4";
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // Alerts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 692);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1222, 739);
            this.MinimumSize = new System.Drawing.Size(1222, 739);
            this.Name = "Alerts";
            this.Text = "Intruder Alert System- All alerts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Alerts_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.alertsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView alertsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartedAt;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoppedAt;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoppedBy;
        private System.Windows.Forms.Button refreshAlertsButton;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Button goToDashboardButton;
        private System.IO.Ports.SerialPort serialPort;
    }
}