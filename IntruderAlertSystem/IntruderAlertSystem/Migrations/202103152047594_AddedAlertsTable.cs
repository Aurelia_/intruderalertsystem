﻿namespace IntruderAlertSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAlertsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        StoppedBy_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.StoppedBy_Id)
                .Index(t => t.StoppedBy_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Alerts", "StoppedBy_Id", "dbo.Users");
            DropIndex("dbo.Alerts", new[] { "StoppedBy_Id" });
            DropTable("dbo.Alerts");
        }
    }
}
