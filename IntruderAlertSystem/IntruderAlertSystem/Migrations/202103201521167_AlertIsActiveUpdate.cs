﻿namespace IntruderAlertSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlertIsActiveUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Alerts", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Alerts", "IsActive");
        }
    }
}
