﻿namespace IntruderAlertSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlertUpdate : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Alerts", name: "StoppedBy_Id", newName: "User_Id");
            RenameIndex(table: "dbo.Alerts", name: "IX_StoppedBy_Id", newName: "IX_User_Id");
            AddColumn("dbo.Alerts", "StopDate", c => c.DateTime(nullable: true));
            DropColumn("dbo.Alerts", "EndDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Alerts", "EndDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Alerts", "StopDate");
            RenameIndex(table: "dbo.Alerts", name: "IX_User_Id", newName: "IX_StoppedBy_Id");
            RenameColumn(table: "dbo.Alerts", name: "User_Id", newName: "StoppedBy_Id");
        }
    }
}
