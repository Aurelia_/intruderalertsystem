﻿using System;

namespace IntruderAlertSystem.ViewModels
{
    public class AlertVm
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StoppedBy { get; set; }
    }
}
