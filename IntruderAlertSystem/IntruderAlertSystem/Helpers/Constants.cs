﻿namespace IntruderAlertSystem.Helpers
{
    public class Constants
    {
        public static string PortName = "COM4";
        public static string ConnectionString = "Server=DESKTOP-BVE50DL; Database=IntruderAlertSystem; Trusted_Connection=True; MultipleActiveResultSets=true";
        public static string SenderEmailAddress = "intruder.notifier@gmail.com";
        public static string SenderPassword = "D8atq0Ms";
        public static string EmailSubject = "Intruder Alert";
        public static string EmailBody = "Hello! Your intruder alert system has been triggered and therefore, the alarm is on. In order to disable it, please connect to your account.";
        public static string SystemOnAlarmOff = "0";
        public static string SystemOnAlarmOn = "1";
        public static string SystemOffAlarmOff = "2";
    }
}
