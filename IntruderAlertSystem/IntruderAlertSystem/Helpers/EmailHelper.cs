﻿using System;
using System.Net;
using System.Net.Mail;

namespace IntruderAlertSystem.Helpers
{
    public class EmailHelper
    {
        public static void Email(string from, string to, string textMessage, string password)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(from);
                message.To.Add(new MailAddress(to));
                message.Subject = Constants.EmailSubject;
                message.IsBodyHtml = false;
                message.Body = textMessage;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(from, password);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
