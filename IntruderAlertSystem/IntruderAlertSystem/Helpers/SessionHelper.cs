﻿using IntruderAlertSystem.Models;

namespace IntruderAlertSystem.Helpers
{
    public static class SessionHelper
    {
        private static Users user;
        private static bool systemIsOn = false;
        private static bool alarmIsOn = false;
        private static string lastLoggedUserEmail = null;
        private static string currentState = "2";

        public static bool UserIsLoggedIn()
        {
            return user != null;
        }

        public static void SetUser(Users newUser)
        {
            user = newUser;
            lastLoggedUserEmail = newUser.Email;
        }

        public static void UnsetUser()
        {
            user = null;
        }

        public static Users getCurrentUser()
        {
            return user;
        }

        public static void ToggleSystemState()
        {
            systemIsOn = !systemIsOn;
        }

        public static bool SystemIsOn()
        {
            return systemIsOn;
        }

        public static void ToggleAlarmState()
        {
            alarmIsOn = !alarmIsOn;
        }

        public static bool AlarmIsOn()
        {
            return alarmIsOn;
        }

        public static string getLastLoggedUserEmail()
        {
            return lastLoggedUserEmail;
        }

        public static string getCurrentState()
        {
            return currentState;
        }

        public static void setCurrentState(string state)
        {
            currentState = state;
        }
    }
}
