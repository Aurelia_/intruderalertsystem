﻿using IntruderAlertSystem.Data;
using IntruderAlertSystem.Models;
using System;
using System.Data.Entity;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntruderAlertSystem.Helpers
{
    public class AlertsHelper
    {
        private static AlertsHelper instance;

        public static AlertsHelper GetInstance()
        {
            if (instance == null)
            {
                return new AlertsHelper();
            }
            else
            {
                return instance;
            }
        }

        private AlertsHelper()
        {
            instance = null;
        }

        public void HandleAlarm()
        {
            var email = SessionHelper.getLastLoggedUserEmail();
            // there is an intruder
            if (email != null)
            {
                var thread_db = new Thread(() => {
                    using (var dbContext = new AppDbContext())
                    {
                        var user = dbContext.ApplicationUsers.FirstOrDefault(u => u.Email.Equals(email));
                        Alerts alert = new Alerts
                        {
                            Id = Guid.NewGuid(),
                            StartDate = DateTime.Now,
                            StopDate = DateTime.Now,
                            User = user,
                            IsActive = true
                        };

                        dbContext.Alerts.Add(alert);
                        dbContext.SaveChanges();
                    }
                });
                thread_db.IsBackground = true;
                thread_db.Start();
                var thread_email = new Thread(() => {
                    try
                    {

                        EmailHelper.Email(Constants.SenderEmailAddress, SessionHelper.getLastLoggedUserEmail(), Constants.EmailBody, Constants.SenderPassword);

                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show("The email wasn't sent!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                });
                thread_email.IsBackground = true;
                thread_email.Start();
            }
        }

        public void UpdateAlarm()
        {
            using (var dbContext = new AppDbContext())
            {
                var user = SessionHelper.getCurrentUser();
                var alarm = dbContext.Alerts.FirstOrDefault(a => a.User.Id.Equals(user.Id) && a.IsActive);
                alarm.StopDate = DateTime.Now;
                alarm.IsActive = false;
                dbContext.SaveChanges();
            }
        }
    }
}
