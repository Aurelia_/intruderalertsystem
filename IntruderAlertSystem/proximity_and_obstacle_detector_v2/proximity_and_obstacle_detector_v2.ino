
// Notes
#define NOTE_C4  262
#define NOTE_G3  196
#define NOTE_A3  220
#define NOTE_B3  247

// pins:
int piezoPin = 8;
int sensorPin = A0;

int numTones = 10;

// notes in the melody
int melody[]={NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4};
int noteDurations[]={4, 8, 8, 4, 4, 4, 4, 4};

/**
 * state = 0 => alarm is off, system on
 * state = 1 => alarm is on, systen on
 * state = 2 => the system is off, alarm is off
 */
int state = 2;

// setup part
void setup() {
  pinMode(sensorPin,INPUT);
  pinMode(piezoPin,OUTPUT);

  Serial.begin(9600);
}

// loop
void loop() {
 if(analogRead(sensorPin) < 250 && state != 2 || state == 1){
     state = 1;
     
  //iterate over the notes of the melody
    for (int thisNote=0; thisNote < 8; thisNote++){
      if(state == 1){
        Serial.println(state);
      }
      //to calculate the note duration, take one second. Divided by the note type
      int noteDuration = 1000 / noteDurations [thisNote];
      tone(piezoPin, melody [thisNote], noteDuration);

      //to distinguish the notes, set a minimum time between them
      //the note's duration +30% seems to work well
      int pauseBetweenNotes = noteDuration * 1.30;
      delay(pauseBetweenNotes);

      //stop the tone playing
      noTone(piezoPin);
    }
 }
 int response = Serial.read();
  if(response == 48){
    // the alarm goes off
    state = 0;
    digitalWrite(piezoPin, LOW);
  }else if (response == 50 && state == 0){
    state = 2;
  }
  Serial.println(state);
}
